import jails from 'jails-js'

export default ( {init, elm, injection, get} )=>{

	const {store, template} = injection
	const render = template( elm )
	const slider = get('slider')

	init(()=>[
		register
	])

	const register = ( {on} )=>{
		on('change', { '.stars input': rating })
		on(':change', { '.slider' :pricing })
		store.subscribe( update )
	}

	const rating = e =>
		store.dispatch('FILTER_RATING', { value: e.target.value })

	const pricing = (e, [min, max]) =>
		store.dispatch('FILTER_PRICE', { min, max })

	const refresh = state =>{
		render( state )
		jails.start( elm )
		slider('refresh')
	}

	const update = ( state, {action} ) =>{
		switch( action ){
			case 'LOAD' :
			case 'FILTER_RATING':
			case 'FILTER_PRICE' :
			case 'FINISH_FILTER_STATE':
				refresh( state )
		}
	}
}
