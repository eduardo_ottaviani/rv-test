import pickmeup from 'pickmeup'

export default ( {init, elm, emit} )=>{

	init(()=>[
		start,
		register
	])

	const start = ()=>
		pickmeup( elm, options )

	const register = ( {on} )=>{
		on('pickmeup-fill', onupdate)
		on('pickmeup-change', onchange)
	}

	const onupdate = e =>
		setTimeout( updateclasses, 100 )

	const getselected = () =>
		$(elm).find('.pmu-days .pmu-selected')
			.addClass('animate')

	const onchange = e =>{

		let [checkin, checkout] = e.detail.date
		let months = pickmeup.defaults.locales['en'].months

		checkin  = `${months[checkin.getMonth()]} ${checkin.getDate()}, ${checkin.getFullYear()}`
		checkout = `${months[checkout.getMonth()]} ${checkout.getDate()}, ${checkout.getFullYear()}`
		checkout = checkin != checkout? checkout : null

		emit(':change', { checkin, checkout })
	}

	const updateclasses = ()=>{

		let selected = getselected()
		let holder = $(elm)

		if(selected.length > 1){
			holder.find('.pmu-days').addClass('multiple')
			selected.last().addClass('last').wrapInner('<span />')
			selected.first().addClass('first')
		}else{
			holder.find('.pmu-days').removeClass('multiple')
		}
	}
}

const render = date =>
	date < now()? { disabled :true } :{}

const now = ()=>{
	const now = new Date()
	now.setHours(0)
	now.setMinutes(0)
	now.setSeconds(0)
	now.setMilliseconds(0)
	return now
}

const options = {
	flat :true,
	mode :'range',
	title_format: 'B / Y',
	first_day : 0,
	render
}

pickmeup.defaults.locales['en'] =
	$.extend(pickmeup.defaults.locales['en'], {
		daysMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
	})
