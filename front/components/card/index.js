export default ( {init, elm} )=>{

	const card = $(elm)

	init(()=>[
		register
	])

	const register = ( {on} )=>{
		on('click', {
			'.-primary' :showhistory,
			'.-primary, .back' :prevent,
			'.back' :hidehistory
		})
	}

	const showhistory = ()=>
		card.addClass('-show-history')

	const hidehistory = ()=>
		card.removeClass('-show-history')

	const prevent = e =>
		e.preventDefault()

}
