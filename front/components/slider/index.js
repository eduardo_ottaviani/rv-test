/* global noUiSlider */
export default ( {init, elm, props, injection, emit} )=>{

	const {store} = injection
	const {min, max} = props('data')
	const control = elm.querySelector('.control')

	const label = {
		min :elm.querySelector('.-min .number'),
		max :elm.querySelector('.-max .number')
	}

	init(( {expose} )=>[
		ready,
		expose( {refresh} )
	])

	const ready = ()=>
		slider
			.then( start )
			.then( listen )

	const start = ()=>{
		let pricing = store.get().pricing
		return noUiSlider.create(control, {
			start: pricing.length? pricing :[min, max],
			range: { min, max }
		})
	}

	const listen = slider =>{
		slider.on('update', update )
		slider.on('change', value => emit(':change', value) )
	}

	const update = ( [min, max] ) =>{
		label.min.innerText = ~~Number(min)
		label.max.innerText = ~~Number(max)
	}

	const refresh = ()=>{
		if( !control.noUiSlider )
			return
		control.noUiSlider.destroy()
		ready()
	}
}

const slider =
	$.getScript('//cdnjs.cloudflare.com/ajax/libs/noUiSlider/10.1.0/nouislider.min.js')
