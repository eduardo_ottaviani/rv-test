export default ( {init, elm, injection} )=>{

	const {store, template} = injection
	const form    = elm.querySelector('form')
	const render  = template( form )

	init(()=>[
		register,
		update
	])

	const register = ( {on} )=>{
		on('submit',  {'form': search })
		on(':change', {'.calendar': onchange })
	}

	const onchange = (e, data)=>
		render( data )

	const update = ()=>
		render()

	const search = e =>{
		const {checkin, checkout, action} = e.target
		if( checkin.value && checkout.value )
			store.dispatch('FETCH', {
				service :action,
				checkin: checkin.value,
				checkout :checkout.value
			})
		e.preventDefault()
	}
}
