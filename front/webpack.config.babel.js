import path from 'path'
import glob from 'glob'
import {optimize} from 'webpack'
import ExtractTextPlugin from 'extract-text-webpack-plugin'

const config = {
	src	: './',
	dist: `${__dirname}/../dist/`,
	publicPath :'./'
}

export default {

	entry :glob.sync(`${config.src}/apps/**/index{.js,.styl}`).reduce(entries,{
		core:[ `${config.src}/core` ]
	}),

	output: {
		path: config.dist,
		filename: '[name]/index.js',
		publicPath: config.publicPath
	},

	resolve:{
		root  :[ path.resolve(config.src), path.resolve(config.dist) ]
	},

	plugins :[
		new optimize.CommonsChunkPlugin('core', 'core.js')
	].concat(
		new optimize.UglifyJsPlugin({ compress :{ warnings:false }, minimize :true }),
		new ExtractTextPlugin('[name]/index.css', {allChunks: false})
	),

	module: {
		loaders: [{
			loader: 'babel',
			test: /\.js$/,
			exclude: /node_modules/,
			query:{
				presets:['es2015']
			}
		},
		{
			test:   /\.styl$/,
			loader: ExtractTextPlugin.extract('css-loader!stylus-loader?paths[]=node_modules&paths[]=./')
		}]
	}
}

function entries(acc, file){
	let dir 	 = path.dirname(file).split(/\//).pop()
	acc[dir] 	 = (acc[dir] || []).concat(file)
	return acc
}
