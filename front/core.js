import jails 	from 'jails-js'
import scriptjs from 'scriptjs'
import logger 	from 'jails.packages/logger'
import adapter  from 'jails.packages/jquery.adapter'

//@Application JS
const main = document.getElementById('main-script')
const app  = main.getAttribute('data-application')

//@CDN libs
const jquery = '//code.jquery.com/jquery-3.2.1.min.js'

scriptjs([ jquery ], ()=>{

	scriptjs([ app ], ()=>{
		jails
			.use( adapter($) )
			.use( logger() )
			.start()
	})
})
