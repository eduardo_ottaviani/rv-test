const body = $('html, body')

export default ( {init, injection} )=>{

	const {store} = injection

	init(()=>[
		register
	])

	const register = ()=>
		store.subscribe( update )

	const animateTo = target =>{
		body.delay( 500 )
			.animate({ scrollTop: target.offset().top}, 1000)
			.promise()
			.then(()=> body.removeClass('-loading') )
	}

	const update = ( state, {action})=>{
		switch( action ){
			case 'FETCH':
				body.addClass('-loading')
				break
			case 'LOAD':
				animateTo( $('.search-results') )
		}
	}
}
