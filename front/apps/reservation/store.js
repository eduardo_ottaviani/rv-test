import simplestore from 'jails.packages/store'
import {R} from './reducers'

let store

export default store = simplestore({
	checkin :'',
	checkout:'',
	data  	:[],
	items 	:[],
	range 	:{},
	rating  :0,
	pricing :[],
	loading :false,
	isfiltering :false
})

store.actions({

	FETCH( state, payload ){

		state.checkin  = payload.checkin
		state.checkout = payload.checkout
		state.loading  = true
		state.rating   = 0

		$.get( payload.service ).then(data => store.dispatch('LOAD', data) )
		return state
	},

	LOAD( state, payload ){

		state.data    = R.items( state.data, payload )
		state.range   = R.range( state.range, payload )
		state.items   = [].concat( state.data )
		state.pricing = [state.range.min, state.range.max]
		state.loading = false

		return state
	},

	FILTER_RATING( state, payload ){

		state.isfiltering = true
		state.rating = +payload.value

		delay(900).then(()=> store.dispatch('FINISH_FILTER_STATE'))
		return state
	},

	FILTER_PRICE( state, payload ){

		state.isfiltering = true
		state.pricing = [~~(+payload.min), ~~(+payload.max)]

		delay(900).then(()=> store.dispatch('FINISH_FILTER_STATE'))
		return state
	},

	FINISH_FILTER_STATE( state, payload ){
		state.items = R.filter( state.data, state )
		state.isfiltering = false
		return state
	}

})

const delay = time => ( {then(fn){ setTimeout(fn, time) } } )

store.subscribe( (state, {action}) => {
	console.info( action, state )
})
