//@ Modules
import jails from 'jails-js'
import store from './store'
import template from 'jails.packages/template'
import {T} from 'jails.packages/template'

//@ Components
import app   from './app'
import maincontent   from 'components/main-content'
import searchresults from 'components/search-results'
import calendar 	 from 'components/calendar'
import slider 	 	 from 'components/slider'
import card 		 from 'components/card'

T.settings.delimiters = ['(', ')']

const dependency = {
	injection : { store, template }
}

//@Main Component
jails('reservation', app, dependency)

//@Components
jails('calendar', calendar, dependency)
jails('slider', slider, dependency)
jails('card', card, dependency)
jails('main-content', maincontent, dependency)
jails('search-results', searchresults, dependency)
