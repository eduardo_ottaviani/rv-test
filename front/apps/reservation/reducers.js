export const R = {

	filter( state = [], payload ){
		return R.rating(
			R.pricing( state, payload ),
			payload
		)
	},

	rating( state = [], payload ){
		return state.filter( item => {
			if( payload.rating == 0) return true
			return item.rate == payload.rating
		})
	},

	pricing( state = [], payload ){
		return state.filter( item => {
			return item.price >= payload.pricing[0] && item.price <= payload.pricing[1]
		})
	},

	range( state, payload ){
		return payload.hotels.reduce( ( {min, max}, item) =>{
			max = ~~(item.price > max? item.price :max)
			min = ~~(item.price < min || min == 0? item.price :min)
			return {min, max}
		}, {min:0, max:0})
	},

	items( state, payload ){
		return payload.hotels.map( item => {
			item.stars = [...Array(item.rate).keys()]
			item.price_history = R.chart( item.price_history, payload )
			return item
		})
	},

	chart( state = [], payload ){
		const max = state.reduce( (curr, {value}) => curr > value? curr :value, 0 )
		return state.map( ({value, month}) => ({
			value 	:~~value,
			label	:month,
			percent :(value * 100) / max
		}) )
	}
}
