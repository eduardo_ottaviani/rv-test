# RV-test

## Setup

Basta fazer o clone do projeto:

`git clone https://bitbucket.org/eduardo_ottaviani/rv-test.git`

---
E instalar as dependências, na pasta `front` do projeto:

`npm install`

---

Após a instalação, uma indicação de que o servidor está rodando aparecerá no terminal mostrando o endereço do site: `http://localhost:3000`

Caso tenha dificuldade para subir o server, basta entrar na pasta `server` e rodar manualmente o comando:

`npm start`


## Back-end

A parte server-side foi separada da pasta front e tem como função:

- Subir o servidor na porta `3000`
- Servir os estáticos, usando o `nunjucks` como template engine
- Servir a api `localhost:3000/service/get-hotels`, servindo o arquivo `json` estático passado na descrição do teste, não há lógica )

## Front-end

- Para a camada base do projeto estou usando a biblioteca `jQuery`.
- Para a camada de aplicação/componentes estou usando o `Jails` na versão 3.
- Para o css estou usando `Stylus` sem nenhuma dependência.
- Para a maioria das dependências de componentes externos estou carregando os scripts de forma assíncrona

- As dependencias do projeto estão sendo carregadas no script `core.js`
- O js da aplicação é exportado para a pasta `dist/` usando como entry point a pasta `apps/{applicacao}/index.js`


## Dependencies & Browser Support

`npm 3+`
`node 6.10+`

Recent versions of :

- `Google Chrome`
- `Android 7 Nougat`
- `Microsoft Edge`
- `Mozilla Firefox`
