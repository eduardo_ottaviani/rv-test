const path = require('path')
const nunjucks = require('nunjucks')
const express = require('express')
const app = express()

const data = require('./data.json')
const root = path.resolve(__dirname, '../')
const port = 3000

app.use( express.static( root ) )

app.set('view engine', 'njk')
app.set('views', root)

const env = nunjucks.configure(['../front'], {
	express   : app,
	autoescape: false,
	watch	  : true
})

/* Routes */
app.get('/favicon.ico', (req, res)=>{
    res.status(204)
})

app.get('/:page', (req, res, next)=>{
	const {page} = req.params
	res.render(`apps/${page}/index`)
})

app.get('/', (req, res) => res.render(`apps/landing/index`))

app.get('/service/get-hotels', (req, res) => {
	res.json( data )
})

app.listen( port )

console.log('Site is ready :', `http://localhost:${port}`)
